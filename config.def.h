/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
#ifdef CENTERED
	static int centered = 1;                    /* -c option; centers dmenu on screen */
#else
	static int centered = 0;                    /* -c option; centers dmenu on screen */
#endif
static int min_width = 700;                    /* minimum width when centered */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[] = "JetBrains Mono:size=10";
static const char *fonts[] = {
	"JetBrains Mono:size=10",
	"FontAwesome:size=10",
	"monospace:size=10",
};

static char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

/* NORD THEME */
// static const char col_background[]     = "#2e3440";
// static const char col_foreground[]     = "#e5e9f0";
// 
// static const char col_dark_black[]     = "#3a4151";
// static const char col_bright_black[]   = "#505a6f";
// static const char col_dark_red[]       = "#884652";
// static const char col_bright_red[]     = "#bf676a";
// static const char col_dark_green[]     = "#708f6b";
// static const char col_bright_green[]   = "#a3be8c";
// static const char col_dark_yellow[]    = "#ab9466";
// static const char col_bright_yellow[]  = "#ebcb8b";
// static const char col_dark_blue[]      = "#4f6696";
// static const char col_bright_blue[]    = "#81a1c1";
// static const char col_dark_magenta[]   = "#85648b";
// static const char col_bright_magenta[] = "#b48ead";
// static const char col_dark_cyan[]      = "#6b91a1";
// static const char col_bright_cyan[]    = "#87bfcf";
// static const char col_dark_white[]     = "#adb0bd";
// static const char col_bright_white[]   = "#e5e9f0";
// 
// static const char col_dark_grey[]      = "#3a4151";
// static const char col_light_grey[]     = "#505a6f";

/* GRUVBOX THEME */
/* static const char col_foreground[] = "#ebdbb2"; */
/* static const char col_background[] = "#272727"; */
/* static const char col_dark_black[] = "#383c3f"; */
/* static const char col_bright_black[] = "#928373"; */
/* static const char col_dark_red[] = "#cc231c"; */
/* static const char col_bright_red[] = "#fb4833"; */
/* static const char col_dark_green[] = "#989719"; */
/* static const char col_bright_green[] = "#b8ba25"; */
/* static const char col_dark_yellow[] = "#d79920"; */
/* static const char col_bright_yellow[] = "#fabc2e"; */
/* static const char col_dark_blue[] = "#448488"; */
/* static const char col_bright_blue[] = "#83a597"; */
/* static const char col_dark_magenta[] = "#b16185"; */
/* static const char col_bright_magenta[] = "#d3859a"; */
/* static const char col_dark_cyan[] = "#689d69"; */
/* static const char col_bright_cyan[] = "#8ec07b"; */
/* static const char col_dark_white[] = "#a89983"; */
/* static const char col_bright_white[] = "#ebdbb2"; */

/* TOMORROW NIGHT THEME */
static const char col_background[] = "#1d1f21";
static const char col_foreground[] = "#c5c8c6";
static const char col_text[] = "#1d1f21";
static const char col_cursor[] = "#ffffff";
static const char col_dark_black[]   = "#1d1f21";
static const char col_dark_red[]     = "#cc6666";
static const char col_dark_green[]   = "#b5bd68";
static const char col_dark_yellow[]  = "#e6c547";
static const char col_dark_blue[]    = "#81a2be";
static const char col_dark_magenta[] = "#b294bb";
static const char col_dark_cyan[]    = "#70c0ba";
static const char col_dark_white[]   = "#373b41";
static const char col_bright_black[]   = "#666666";
static const char col_bright_red[]     = "#ff3334";
static const char col_bright_green[]   = "#9ec400";
static const char col_bright_yellow[]  = "#f0c674";
static const char col_bright_blue[]    = "#81a2be";
static const char col_bright_magenta[] = "#b77ee0";
static const char col_bright_cyan[]    = "#54ced6";
static const char col_bright_white[]   = "#282a2e";

static char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { col_foreground, col_background },
	[SchemeSel]  = { col_background, col_bright_green },
	[SchemeOut]  = { col_background, col_bright_green },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
#ifdef CENTERED
	static unsigned int lines      = 22;
#else
	static unsigned int lines      = 0;
#endif

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
#ifdef CENTERED
	static const unsigned int border_width = 3;
#else
	static const unsigned int border_width = 0;
#endif

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
	{ "font",        STRING, &font },
	{ "normfgcolor", STRING, &col_foreground },
	{ "normbgcolor", STRING, &col_background },
	{ "selfgcolor",  STRING, &col_background },
	{ "selbgcolor",  STRING, &col_dark_blue },
	{ "prompt",      STRING, &prompt },
};

static unsigned int lineheight = 24;
static unsigned min_lineheight = 14;
